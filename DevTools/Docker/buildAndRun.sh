#!/bin/bash
dockerImageName=$MESHGATEWAY_DOCKER_FQN
docker build -t $dockerImageName .
docker run -p 4000:4000 --name meshgateway $dockerImageName