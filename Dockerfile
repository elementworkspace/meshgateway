FROM node:lts-alpine3.14
COPY /gateway /data
RUN chgrp node /data && chown node /data
WORKDIR /data
USER node
RUN yarn add graphql @graphql-mesh/runtime @graphql-mesh/cli @graphql-mesh/openapi @graphql-mesh/graphql


#TODO: Multi-stage building
#FROM base as dev
#COPY --from=base /data .
EXPOSE 4000
#CMD if [ -f "init.sh" ]; then sh init.sh; fi && yarn && yarn graphql-mesh build && yarn graphql-mesh dev
CMD if [ -f "init.sh" ]; then sh init.sh; fi && yarn graphql-mesh dev